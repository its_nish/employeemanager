import { HttpClient, HttpClientModule } from '@angular/common/http'; 
import {Injectable} from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from '../models/employee.model';


@Injectable()
export class DataService {

  private http: HttpClient;
   
  constructor(private httpClient: HttpClient) {
      this.http = httpClient;
  }
   
   getEmployeeList(): Observable<any>{
    return this.http.get("./assets/sample.json")
   }  
   
}