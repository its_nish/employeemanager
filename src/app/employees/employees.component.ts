import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Employee } from '../models/employee.model';
import { DataService } from '../services/DataService';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit{
  
  employeeList: Employee[] = [];

  currentEmployeeList: Employee[] = [];

  startIndex = 0; endIndex = 5; pageNumber = 0;

  disablePrev = true; disableNext = false;

  show = false;

  constructor(private db: DataService) {
   }

   ngOnInit(){
    this.db.getEmployeeList().subscribe((data) => {
      this.employeeList = data;
      this.currentEmployeeList = this.employeeList.slice(this.startIndex,this.endIndex);
    });
   }

   addEmployee(value: any){
    console.log(value);
    this.employeeList.push(value);
    this.show=false;
   }

   showHide(){
     this.show = true;
   }

   onNext(){
     this.startIndex = this.startIndex+5;
     this.endIndex = this.endIndex+5;
     this.currentEmployeeList = this.employeeList.slice(this.startIndex,this.endIndex);
     if(this.endIndex >= this.employeeList.length){
       this.disableNext = true;
     }
     this.disablePrev = false;
     this.pageNumber++;
   }

   onPrev(){
     this.startIndex = this.startIndex -5;
     this.endIndex = this.endIndex -5;
     this.currentEmployeeList = this.employeeList.slice(this.startIndex,this.endIndex);
     if(this.startIndex == 0){
       this.disablePrev = true;
     }
     this.disableNext = false;
     this.pageNumber--;
   }

   

}
