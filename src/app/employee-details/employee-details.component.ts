import { APP_INITIALIZER, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Employee } from '../models/employee.model';
import { DataService } from '../services/DataService';

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html',
  styleUrls: ['./employee-details.component.css']
})
export class EmployeeDetailsComponent implements OnInit {

  employeeList: Employee[] = [];
  employeeDetails:Employee = {
    id: 0,
    name: "XYZ",
    email: "xyz@gmail.com",
    phone:"0123456789"
  };
  employeeId:number = 0;

  constructor(private db: DataService,private activatedRoute: ActivatedRoute) {
    
   }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe(data => {
      this.employeeId = data.id;
      this.fillEmployeeDetails(this.employeeId);
    }
    )
  }

  fillEmployeeDetails(id:any){
    this.db.getEmployeeList().subscribe((data) => {
      this.employeeList = data;
      var i: number;
    for(i=0;i<this.employeeList.length;i++){
      if(id == this.employeeList[i].id){
        this.employeeDetails = this.employeeList[i];
      }
    }
    });
    
  }

}
