import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  template: `
 <nav class="navbar navbar-light" style="background-color: #e3f2fd;">
  <a class="navbar-brand align" href="#">
    <img src="https://www.pngarts.com/files/3/Letter-A-PNG-Image-Background.png" width="30" height="30"  class="d-inline-block align-top" alt="">
    Employee Manager 
  </a>
  
 </nav>
  `,
  styles: [`
  .align{
    display:flex;
    align-items:center;
  }
  `]
})
export class HeaderComponent implements OnInit {
 
    constructor() {
    }
    
      ngOnInit(): void {
      }
}
