import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  @Output() newEmployee: EventEmitter<any> = new EventEmitter();
  employeeUpdateForm: FormGroup;

  control = {
    updateForm: {
      name: new FormControl(null , [Validators.required]),
      email: new FormControl(null , [Validators.required]),
      phone: new FormControl(null , [Validators.required]),
    }
  }

  constructor() {
    this.employeeUpdateForm = new FormGroup(this.control.updateForm);
   }

  ngOnInit(): void {
  }

  submit(){
    this.newEmployee.emit(this.employeeUpdateForm.value);
    console.log(this.employeeUpdateForm.value);
  }



}
